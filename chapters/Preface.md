---
title: Preface
---

This book is intended to be read along with or just after an introductory book on particle physics. Here I intend to travel through time and describe the history of tau lepton using real research articles. With additional comments to fill the gaps, one can learn:
	- to read and understand research articles;
	- statistical methods used in experimental particle physics analysis;
	- physics of the detectors and colliders used these experiments;
	- theoretical developments over the time.

I chose to write this book to observe the impact of (historical) narrative in learning, primarily in particle physics.
