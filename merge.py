#! python3
# merge.py - Combines all the individual PDFs into a single PDF.

import os, sys
from PyPDF2 import PdfFileMerger, PdfFileReader

pdfFiles = ['chapters/Preface.pdf',
            'raw/slac-pub-1626.pdf']

merger = PdfFileMerger()
for filename in pdfFiles:
    merger.append(PdfFileReader(open(filename, 'rb')))

merger.write("book-original.pdf")
