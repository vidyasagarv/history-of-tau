# History of tau

A collection of important papers in the history of [tau]( https://wikipedia.org/wiki/Tau_(particle)) with addtional comments in the form of a book.

I want to write a book intended for undergraduate students to learn fundamentals of particle physics; both theoretical and experimental (statistics, detector physics) aspects in the form of a history lesson. The Code Book by Simon Singh taught me the value of (historical) narrative in learning and I want to try to do the same for particle physics, but with real research papers.

## TO-DO

- Design a cover
- Increase font size in pandoc-compiled pdfs


## Requirements to compile this project

- [pandoc](pandoc.org): To convert markdown files to pdf using LaTeX engine
- [PyPDF2](https://github.com/mstamy2/PyPDF2): Python library to split and merge pdfs
